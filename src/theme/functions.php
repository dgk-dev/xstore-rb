<?php

/** analytics */
require(get_stylesheet_directory().'/includes/analytics.php');

/** Cambios en tienda */
require(get_stylesheet_directory().'/includes/store-changes.php');

/** Cambios en grid de productos y slider */
require(get_stylesheet_directory().'/includes/etheme-product.php');
require(get_stylesheet_directory().'/includes/etheme-slider.php');

/** funciones de búsqueda */
require(get_stylesheet_directory().'/includes/search-results.php');

/** funciones de encuesta de satisfacción */
require(get_stylesheet_directory().'/includes/survey.php');

/** sobreescribe grid de brands para mejor maquetación */
require(get_stylesheet_directory().'/includes/brand-list.php');

/** Funciones de chat */
require(get_stylesheet_directory().'/includes/chat.php');

/** Filtro por categorías */
require(get_stylesheet_directory().'/includes/category-filter.php');

/** Estilos de login */
require(get_stylesheet_directory().'/includes/login-style.php');

add_action('wp_enqueue_scripts', 'rb_resources');
function rb_resources(){
    etheme_child_styles();
    wp_register_script('rebloom-functions', get_stylesheet_directory_uri() . '/js/footer-bundle.js', null, '1.0', true);
	// wp_localize_script('rebloom-functions', 'rebloomGlobalObject', array(
	// 	'add_cart_button_script' => is_shop() || is_product_category() || is_product_tag(),
	// ));
    wp_enqueue_script('rebloom-functions');
}

//langs: themes and plugins
add_action( 'after_setup_theme', 'rb_theme_lang' );
function rb_theme_lang() {
    load_child_theme_textdomain( 'xstore', get_stylesheet_directory() . '/languages' );
    
    //plugins
    unload_textdomain('xstore-core');
    load_textdomain('xstore-core', get_stylesheet_directory() . '/languages/xstore-core-es_ES.mo');
}

// functions to change email data
add_filter( 'wp_mail_from', 'rb_sender_email' );
add_filter( 'wp_mail_from_name', 'rb_sender_name' , 50);
function rb_sender_email( $original_email_address ) {
    return 'noreply@rebloomofficial.com';
}
function rb_sender_name( $original_email_from ) {
    return 'Rebloom';
}

/**
 * Gestión de roles shop_sales = ventas : dado de alta desde plugin
 */
add_action( 'wp_before_admin_bar_render', 'rb_admin_bar_remove_logo', 0 );
function rb_admin_bar_remove_logo() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu( 'wp-logo' );
}
add_action( 'admin_menu', 'rb_remove_menu_pages', 999);
function rb_remove_menu_pages() {
  global $current_user;
   
  $user_roles = $current_user->roles;
  
  $roles = array('pedidos', 'vender', 'productos');
  
  if(count(array_intersect($user_roles, $roles))){
    $remove_submenu = remove_submenu_page('woocommerce', 'wc-settings');
    $remove_submenu = remove_submenu_page('woocommerce', 'wc-addons');
    $remove_submenu = remove_submenu_page('woocommerce', 'wc-status');
    $remove_menu = remove_menu_page('woocommerce-marketing');
  }

  $roles = array('blog', 'citas');
  if(count(array_intersect($user_roles, $roles))){
    $remove_menu = remove_menu_page('edit.php?post_type=testimonials');
    $remove_menu = remove_menu_page('edit.php?post_type=staticblocks');
    $remove_menu = remove_menu_page('edit.php?post_type=etheme_portfolio');
    $remove_menu = remove_menu_page('wpcf7');
    $remove_menu = remove_menu_page('tools');
    $remove_menu = remove_menu_page('vc-welcome');
  }
  $roles = array('blog');
  if(count(array_intersect($user_roles, $roles))){
    $remove_menu = remove_menu_page('easy_app_top_level');
  }
}

/**
 * Cambiar mensaje de error del formulario quiero vender
 */
add_filter( 'gettext', 'rb_change_cf7_dropuploader_strings', 20, 3 );
function rb_change_cf7_dropuploader_strings( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
        case 'Archivos más grandes que':
            $translated_text = __( 'El peso máximo permitido por imagen es de', $domain );
        break;
        case 'no está permitido':
            $translated_text = __( '', $domain );
        break;
        case 'Tu pedido ya no se puede cancelar. Por favor, ponte en contacto con nosotros si necesitas ayuda.':
            $translated_text = __( 'Tu pedido está en proceso de cancelación, esto puede tardar. Por favor, ponte en contacto con nosotros si necesitas ayuda.', $domain );
        break;
        
    }
    return $translated_text;
}

/**
 * Agregar búsqueda por metadata al administrador de wordpress
 */
function rb_request_query( $query_vars ) {

	global $typenow;
	global $wpdb;
	global $pagenow;

	if ( 'product' === $typenow && isset( $_GET['s'] ) && 'edit.php' === $pagenow ) {
		$search_term            = esc_sql( sanitize_text_field( $_GET['s'] ) );
		$meta_keys               = array('rb-product-owner');
		$post_types             = array( 'product', 'product_variation' );
		$search_results         = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT DISTINCT posts.ID as product_id, posts.post_parent as parent_id FROM {$wpdb->posts} posts LEFT JOIN {$wpdb->postmeta} AS postmeta ON posts.ID = postmeta.post_id WHERE postmeta.meta_key IN ('" . implode( "','", $meta_keys ) . "') AND postmeta.meta_value LIKE %s AND posts.post_type IN ('" . implode( "','", $post_types ) . "') ORDER BY posts.post_parent ASC, posts.post_title ASC",
				'%' . $wpdb->esc_like( $search_term ) . '%'
			)
		);
		$product_ids            = wp_parse_id_list( array_merge( wp_list_pluck( $search_results, 'product_id' ), wp_list_pluck( $search_results, 'parent_id' ) ) );
		$query_vars['post__in'] = array_merge( $product_ids, $query_vars['post__in'] );
	}

	return $query_vars;
}

add_filter( 'request', 'rb_request_query', 20 );

add_action('wpcf7_mail_sent', function ($cf7) {
    $submission = WPCF7_Submission::get_instance();

    if ( $submission ) {
        $posted_data = $submission->get_posted_data();
        $update_id = false;
        foreach($posted_data as $key=>$value){
            if($key=='cf7du_submission_id') $update_id = true;
        }
        
        if(!$update_id) return;
        //get the current counter
        $val = get_post_meta( $cf7->id, "cf7_submission_id_COUNTER",true);
        
        //If there is no current counter
        if ($val == ""){
            //store the value as received from the form
            $val = 1;
        }else{
            $val += 1;
        }
        //Save the value
        update_post_meta($cf7->id, "cf7_submission_id_COUNTER", $val ); 
    }	
});