<?php
//Whatsapp
add_action('wp_footer','rb_add_footer_whatsapp');
function rb_add_footer_whatsapp(){
	$tel = "5215531767892";

	$url = "https://wa.me/${tel}";
	$img = get_stylesheet_directory_uri().'/img/whatsapp-icon.svg';
	echo "<div id='float-whatsapp'>";
	echo "<strong id='float-whatsapp-text' style=''>Escríbenos por Whatsapp</strong><a href=${url} target='_blank'>";
	echo "<img id='float-whatsapp-img' src='${img}' alt='whatsapp-icon' />";
	echo " </a>";
	echo "</div>";
}
