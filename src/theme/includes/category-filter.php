<?php
include_once('WC_Widget_Layered_Nav_Categories.php');
register_widget('ETC\App\Models\Widgets\Categories_Filter');

add_filter('woocommerce_widget_get_current_page_url', 'rb_add_filter_vars');

function rb_add_filter_vars($link){
    if(is_woocommerce()){
        $filter_vars = array('product_cat', 'filter_brand', 'filter_color', 'filter_material');
        foreach($filter_vars as $var){
            if ( !empty( $_GET[$var] ) ) {
                $link = add_query_arg( $var, $_GET[$var], $link );
            }
        }
    }
    return $link;
}