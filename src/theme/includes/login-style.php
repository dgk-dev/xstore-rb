<?php
/**
 * Custom login screen
 */
add_action('wp_dashboard_setup', 'rb_dashboard_widgets');
function rb_dashboard_widgets() {
    global $wp_meta_boxes;
    wp_add_dashboard_widget('custom_help_widget', 'Bienvenido', 'custom_dashboard_message');
}
 
function custom_dashboard_message() {
    echo '<div style="padding:20px 0; text-align:center;"><img src="'.get_stylesheet_directory_uri().'/img/rebloom-logo.png"></div><p><strong>Este es el administrador del sitio Rebloom</strong>';
}

add_action( 'login_enqueue_scripts', 'rb_login_logo' );
function rb_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/rebloom-logo.png');
            height: 200px;
            width: 200px;
            background-size: 200px 200px;
            background-repeat: no-repeat;
        	padding-bottom: 1em;
        }
    </style>
<?php }

add_filter( 'login_headerurl', 'rb_loginlogo_url');
function rb_loginlogo_url($url) {
    return home_url();
}