<?php

//mensaje extra en recuperar password
add_action('woocommerce_after_lost_password_confirmation_message', 'gv_after_lost_password_confirmation_message');
function gv_after_lost_password_confirmation_message(){
	echo "<p><strong>Si no lo encuentras en la bandeja de entrada, busca en tu bandeja de spam o correo no deseado.</strong></p>";
}

//redirigir a carrito si está logueado a su carrito no está vacío
add_filter( 'woocommerce_login_redirect', 'rb_redirect_login', 10, 2 );
function rb_redirect_login( $redirect, $user ) {

    $saved_cart = '';

    $saved_cart_meta = get_user_meta( $user->ID, '_woocommerce_persistent_cart_' . get_current_blog_id(), true );

    if ( isset( $saved_cart_meta[ 'cart' ] ) ) {
        $saved_cart = array_filter( (array) $saved_cart_meta[ 'cart' ] );
    }

    if ( $saved_cart || !WC()->cart->is_empty()) {
        $redirect = wc_get_cart_url().'?cart_message=1';
	}
	
    return $redirect;
}

//FB
add_action('template_redirect', 'rb_facebook_redirect_login');
function rb_facebook_redirect_login() {
    if( is_user_logged_in() && ! WC()->cart->is_empty() && is_account_page() && ! empty($_GET['opauth']))
        wp_redirect( wc_get_cart_url().'?cart_message=1' );
}

add_action('woocommerce_after_cart_contents', 'rb_add_cart_promo', 10);
function rb_add_cart_promo(){
	if(isset($_GET['cart_message']) && $_GET['cart_message']):
	?>
	<div id="cart-promo-popup" class="white-popup mfp-hide">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="text-center">COMPLETA TU COMPRA</h2>
					<p class="text-center">Detectamos que aún tienes productos en tu carrito. Completa tu compra antes de que se los lleven</p>
				</div>
			</div>
		</div>
	</div>
	<script>
		(function($) {
			$(window).load(function () {
				// retrieved this line of code from http://dimsemenov.com/plugins/magnific-popup/documentation.html#api
				$.magnificPopup.open({
					items: {
						src: '#cart-promo-popup',
						type: 'inline'
					},
					callbacks: {
						open: function() {
							$('.mfp-content').addClass('rb-cart-popup');
						// Will fire when this exact popup is opened
						// this - is Magnific Popup object
						}
					}

				// You may add options here, they're exactly the same as for $.fn.magnificPopup call
				// Note that some settings that rely on click event (like disableOn or midClick) will not work here
				}, 0);
			});
		})(jQuery);
	</script>
		
	<?php
	endif;
}

add_filter( 'woocommerce_get_availability', 'rb_custom_get_availability', 1, 2);
function rb_custom_get_availability( $availability, $_product ) {
  
    // Change Out of Stock Text
    if ( ! $_product->is_in_stock() ) {
        $availability['availability'] = 'SOLD';
    }
    return $availability;
}

//Ocultar productos que no están en stock de los loops principales
add_action( 'pre_get_posts', 'rb_hide_no_stock_products' );
function rb_hide_no_stock_products( $query ){
    if( ! is_admin() && is_post_type_archive( 'product' ) && $query->is_main_query() ){
        $query->set( 'meta_key', '_stock_status' );
		$query->set( 'meta_value', 'instock' );
    }
}

//Redireccionar páginas de marcas a tienda con filtros
function rb_brand_page_redirect() {
    if (is_tax( 'brand' )) {
        global $post;
        $terms = get_the_terms( $post->ID, 'brand' );
        if(!empty($terms)){
            $brandURL = get_permalink( wc_get_page_id( 'shop' ) ).'?orderby=date&filter_brand='.$terms[0]->slug;
            wp_redirect( $brandURL );
            exit;
        }
    }
}
add_action('wp', 'rb_brand_page_redirect');

/**
 * Validar teléfono a 10 dígitos
 */

add_filter( 'woocommerce_checkout_fields', 'rb_remove_default_phone_validation' );
function rb_remove_default_phone_validation( $fields ){
    unset( $fields['billing']['billing_phone']['validate'] );
	return $fields;
}
add_filter( 'woocommerce_billing_fields' , 'rb_custom_override_checkout_fields' );
function rb_custom_override_checkout_fields( $fields ) {
    $fields['billing_phone']['maxlength'] = 10;    
    return $fields;
}
add_action('woocommerce_checkout_process', 'rb_custom_validate_billing_phone');
function rb_custom_validate_billing_phone() {
    $is_correct = preg_match('/^[0-9]{10}$/', $_POST['billing_phone']);
    if ( $_POST['billing_phone'] && !$is_correct) {
        wc_add_notice( __( 'El número de teléfono tiene que ser <strong>de 10 dígitos</strong>.' ), 'error' );
    }
}

/**
 * Agregar campos de colonia e instagram
 */

add_filter( 'woocommerce_billing_fields', 'rb_custom_woocommerce_billing_fields' );

function rb_custom_woocommerce_billing_fields( $fields ) {

    $billing_address_3 = array(
        // 'label'       => __('Colonia', 'woocommerce'),             // Add custom field label
        'placeholder' => _x('Colonia', 'placeholder', 'woocommerce'),  // Add custom field placeholder
        'required'    => true,             // if field is required or not
        'clear'       => false,             // add clear or not
        'type'        => 'text',                // add field type
    );

    $fields = array_slice($fields, 0, 6, true) +
    array("billing_address_3" => $billing_address_3) +
    array_slice($fields, 6, count($fields) - 1, true);
    
    $billing_instagram = array(
        'label'       => __('Nombre de usuario de Instagram', 'woocommerce'),             // Add custom field label
        // 'placeholder' => _x('Instagram', 'placeholder', 'woocommerce'),  // Add custom field placeholder
        'required'    => false,             // if field is required or not
        'clear'       => false,             // add clear or not
        'type'        => 'text',                // add field type
    );

    $fields = array_slice($fields, 0, 11, true) +
    array("billing_instagram" => $billing_instagram) +
    array_slice($fields, 11, count($fields) - 1, true);

    return $fields;
}

// Printing the Billing Address on My Account
add_filter( 'woocommerce_my_account_my_address_formatted_address', 'rb_my_account_address_formatted_address', 10, 3 );
function rb_my_account_address_formatted_address( $fields, $customer_id, $type ) {

    $fields['address_3'] = get_user_meta( $customer_id, '_billing_address_3', true );
    $fields['instagram'] = get_user_meta( $customer_id, '_billing_instagram', true );

    return $fields;
}

// Checkout -- Order Received (printed after having completed checkout)
add_filter( 'woocommerce_order_formatted_billing_address', 'rb_add_data_formatted_billing_address', 10, 2 );
function rb_add_data_formatted_billing_address( $fields, $order ) {
    $fields['address_3'] = $order->get_meta('_billing_address_3');
    $fields['instagram'] = $order->get_meta('_billing_instagram');

    return $fields;
}
add_filter( 'woocommerce_order_formatted_shipping_address', 'rb_add_data_formatted_shipping_address', 10, 2 );
function rb_add_data_formatted_shipping_address( $fields, $order ) {
    $fields['address_3'] = $order->get_meta('_billing_address_3');
    return $fields;
}

// Creating merger VAT variables for printing formatting
add_filter( 'woocommerce_formatted_address_replacements', 'rb_formatted_address_replacements', 10, 2 );
function rb_formatted_address_replacements( $replacements, $args  ) {
    $replacements['{address_3}'] = ! empty($args['address_3']) ? $args['address_3'] : '';
    $replacements['{address_3_upper}'] = ! empty($args['address_3']) ? strtoupper($args['address_3']) : '';
    
    $replacements['{instagram}'] = ! empty($args['instagram']) ? $args['instagram'] : '';
    $replacements['{instagram_upper}'] = ! empty($args['instagram']) ? strtoupper($args['instagram']) : '';

    return $replacements;
}

//Defining the Spanish formatting to print the address, including VAT.
add_filter( 'woocommerce_localisation_address_formats', 'rb_custom_localisation_address_format' );
function rb_custom_localisation_address_format( $formats ) {
    foreach($formats as $country => $string_address ) {
        $formats[$country] = str_replace('{address_2}', '{address_2} Colonia: {address_3}', $string_address);
    }
    return $formats;
}

add_filter( 'woocommerce_customer_meta_fields', 'rb_custom_customer_meta_fields' );
function rb_custom_customer_meta_fields( $fields ) {
    $fields['billing']['fields']['billing_address_3'] = array(
        'label'       => __( 'Colonia', 'woocommerce' )
    );

    return $fields;
}

add_filter( 'woocommerce_admin_billing_fields', 'rb_custom_admin_billing_fields' );
function rb_custom_admin_billing_fields( $fields ) {
    $fields['address_3'] = array(
        'label' => __( 'Colonia', 'woocommerce' ),
        'show'  => false
    );
    $fields['instagram'] = array(
        'label' => __( 'Usuario de instagram', 'woocommerce' ),
        'show'  => true
    );

    return $fields;
}
add_filter( 'woocommerce_admin_shipping_fields', 'rb_custom_admin_shipping_fields' );
function rb_custom_admin_shipping_fields( $fields ) {
    $fields['address_3'] = array(
        'label' => __( 'Colonia', 'woocommerce' ),
        'show'  => false
    );

    return $fields;
}

add_filter( 'woocommerce_found_customer_details', 'rb_custom_found_customer_details' );
function rb_custom_found_customer_details( $customer_data ) {
    $customer_data['billing_address_3'] = get_user_meta( $_POST['user_id'], '_billing_address_3', true );
    $customer_data['billing_instagram'] = get_user_meta( $_POST['user_id'], '_billing_instagram', true );

    return $customer_data;
}

add_filter( 'woocommerce_email_order_meta_fields', 'rb_custom_woocommerce_email_order_meta_fields', 10, 3 );

function rb_custom_woocommerce_email_order_meta_fields( $fields, $sent_to_admin, $order ) {
    $fields['meta_key'] = array(
        'label' => __( 'Usuario de Instagram' ),
        'value' => get_post_meta( $order->get_id(), '_billing_instagram', true ),
    );
    return $fields;
}

add_filter('woocommerce_order_details_before_order_table', 'rb_add_items_count_on_order_page');

function rb_add_items_count_on_order_page($order){
    $billing_instagram = get_post_meta( $order->get_id(), '_billing_instagram', true );
    if(!$billing_instagram) return;
   ?>
   <div class="woocommerce-order-overview-wrapper">
       <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">
           <li class="woocommerce-order-overview__instgram instagram">
               Usuario de Instagram: <strong><?php echo $billing_instagram ?></strong>
           </li>
       </ul>
   </div>
   <?php
}

/**
 * Reescribe etiquetas de "facturación" a "envío" forzando el envío a la dirección de facturación en woocommerce.
 */
add_filter( 'gettext', 'rb_change_billing_field_strings', 20, 3 );
function rb_change_billing_field_strings( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
        case 'Detalles de Facturación':
        case 'Dirección de facturación':
        case 'Facturación &amp; Envío':
            $translated_text = __( 'Dirección de envío', 'woocommerce' );
        break;
        
    }
    return $translated_text;
}

add_filter( 'woocommerce_add_error', 'rb_change_wc_errors' );
function rb_change_wc_errors( $error ) {
    if ( strpos( $error, 'Billing ' ) !== false || strpos( $error, 'Facturación ' ) !== false ) {
        $error = str_replace("Billing ", "", $error);
        $error = str_replace("Facturación ", "", $error);
    }
    return $error;
}

add_filter( 'woocommerce_layered_nav_count_maybe_cache', function(){
    return false;
} );

/**
 * Agregar size a grid de producto
 */

add_action('et_after_shop_loop_title', 'rb_add_product_size', 10);
function rb_add_product_size(){
    global $product;
    echo $product->get_attribute('pa_size') ? '<strong>Size: </strong>'.$product->get_attribute('pa_size') : '';

}

/**
 * Reducir Stock en estatus de orden pendiente 
 */

add_action( 'woocommerce_checkout_order_processed', 'rb_reduce_stock_on_pending', 20, 3 );
function rb_reduce_stock_on_pending($order_id, $posted_data, $order){
    
    if ( ! get_option('woocommerce_manage_stock') == 'yes' && ! sizeof( $order->get_items() ) > 0 ) {
        return;
    }
    
    //   $paymethod = $order->payment_method_title;
    //   $orderstat = $order->get_status();

    if( $order->has_status('pending') ){
        foreach ( $order->get_items() as $item ) {
            $product_id = $item->is_type('variable') ? $item->get_variation_id() : $item->get_product_id();
            $item_qty = $item->get_quantity();
            $product = wc_get_product( $product_id );
    
            // $product_stock = $product->get_stock_quantity();

            $result = wc_update_product_stock( $product, $item_qty, 'decrease' );
        }
        $order->update_meta_data( '_stock_reduced', 'yes' );
        $order->save();
    }
}

add_action( 'woocommerce_order_status_pending_to_cancelled', 'rb_restore_order_stock', 10, 1 );
add_action( 'woocommerce_order_status_on-hold_to_cancelled', 'rb_restore_order_stock', 10, 1 );
function rb_restore_order_stock( $order_id ) {
    
    $order = wc_get_order($order_id);
    
    if ( ! get_option('woocommerce_manage_stock') == 'yes' && ! sizeof( $order->get_items() ) > 0 ) {
        return;
    }
    
    if($order->get_meta('_stock_reduced') != 'yes') return;
    
    if(!is_admin()) WC()->cart->empty_cart();
    
    foreach ( $order->get_items() as $item ) {
        $product_id = $item->is_type('variable') ? $item->get_variation_id() : $item->get_product_id();

        $product = wc_get_product( $product_id );
    
        $product_stock = $product->get_stock_quantity();

        $result = $product_stock != 1 ? wc_update_product_stock( $product, 1, 'set' ) : null;
    }

    $order->update_meta_data( '_stock_reduced', 'restored' );
    $order->save();
}

// Deshabilita una segunda reducción de stock si ya se hizo en la función rb_reduce_stock_on_pending
add_filter( 'woocommerce_can_reduce_order_stock', 'rb_disable_reduce_stock', 15, 2 );
function rb_disable_reduce_stock( $reduce_stock, $order ) {
	return $order->get_meta('_stock_reduced') == 'yes' ? false : $reduce_stock;
}

/**
 * Agregar campo Propietario en productos
 */
add_action( 'admin_menu', 'rb_product_data_metabox' );

function rb_product_data_metabox() {
   add_meta_box('rb-product-data', // meta box ID
       'Detalles de Producto', // meta box title
       'rb_print_data_metabox', // callback function that prints the meta box HTML 
       'product', // post type where to add it
       'normal', // priority
       'high' ); // position
}

/*
* Meta Box HTML
*/
function rb_print_data_metabox( $post ) {
    $owner = get_post_meta($post->ID, 'rb-product-owner', true); 
	$nonce = wp_create_nonce(basename(__FILE__));
   ?>
   <input type="hidden" name="rb_product_data_nonce" value="<?php echo $nonce; ?>">
    <table style="width: 100%;">
        <tbody class="form-table">
            <tr>
                <th style="font-weight:normal">
                    <label for="rb-product-owner">Propietario</label>
                </th>
                <td>
                    <input name="rb-product-owner" type="text" id="rb-product-owner" value="<?php echo $owner; ?>" />
                </td>
            </tr>
        </tbody>
    </table>
   <?php
}

/*
* Save Meta Box data
*/
add_action('save_post', 'rb_product_data_save');

function rb_product_data_save( $post_id ) {
    // verify nonce
    if (!isset($_POST['rb_product_data_nonce']) || !wp_verify_nonce($_POST['rb_product_data_nonce'], basename(__FILE__)))
        return $post_id;

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;

    // check permissions
    if ('product' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }  

    $oldowner = get_post_meta($post_id, "rb-product-owner", true);
    $newowner = $_POST["rb-product-owner"]; 
    if ($newowner != $oldowner) {
        update_post_meta($post_id, "rb-product-owner", $newowner);
    } 
}

/**
 * Agregar Propietario en quick edit
 */

add_filter( 'manage_edit-product_columns', 'rb_add_product_columns' );
function rb_add_product_columns( $columns ) {
    $columns['rb-product-owner'] = 'Propietario';
    return $columns;
}

add_action( 'manage_product_posts_custom_column', 'rb_product_columns_content', 10, 2 );
function rb_product_columns_content( $column_name, $post_id ) {
    if ( 'rb-product-owner' != $column_name ) {
        return;
    }
 
    $rb_product_owner = get_post_meta( $post_id, 'rb-product-owner', true );
    echo $rb_product_owner;
}

add_action('quick_edit_custom_box',  'rb_quick_edit_fields', 10, 2);
function rb_quick_edit_fields( $column_name, $post_type ) {
 
	// you can check post type as well but is seems not required because your columns are added for specific CPT anyway
 
	switch( $column_name ) :
		case 'rb-product-owner': {
 
			// you can also print Nonce here, do not do it ouside the switch() because it will be printed many times
			$nonce = wp_create_nonce(basename(__FILE__));
 
			// please note: the <fieldset> classes could be:
			// inline-edit-col-left, inline-edit-col-center, inline-edit-col-right
			// each class for each column, all columns are float:left,
			// so, if you want a left column, use clear:both element before
			// the best way to use classes here is to look in browser "inspect element" at the other fields
 
            // for the FIRST column only, it opens <fieldset> element, all our fields will be there
			echo '<fieldset class="inline-edit-col-center">
                    <div class="inline-edit-col">
                        <input type="hidden" name="rb_product_data_nonce" value="'.$nonce.'">
                        <label>
                            <span class="title">Propietario</span>
                            <span class="input-text-wrap"><input type="text" name="rb-product-owner" value=""></span>
                        </label>  
                    </div>
                </fieldset>';
			break;
 
		}
	endswitch;
}

add_action( 'admin_footer', 'rb_quick_edit_javascript' );
function rb_quick_edit_javascript() {
    global $current_screen;
    if ($current_screen->action == 'add' || $current_screen->post_type != 'product') {
        return;
    }
?>
    <script type="text/javascript">
        jQuery(function($){
    
            // it is a copy of the inline edit function
            var wp_inline_edit_function = inlineEditPost.edit;

            // we overwrite the it with our own
            inlineEditPost.edit = function( post_id ) {

                // let's merge arguments of the original function
                wp_inline_edit_function.apply( this, arguments );

                // get the post ID from the argument
                var id = 0;
                if ( typeof( post_id ) == 'object' ) { // if it is object, get the ID number
                    id = parseInt( this.getId( post_id ) );
                }

                //if post id exists
                if ( id > 0 ) {

                    // add rows to variables
                    var specific_post_edit_row = $( '#edit-' + id ),
                        specific_post_row = $( '#post-' + id ),
                        product_owner = $( '.column-rb-product-owner', specific_post_row ).text();
                    // populate the inputs with column data
                    $( ':input[name="rb-product-owner"]', specific_post_edit_row ).val( product_owner );
                }
            }
        });
    </script>
<?php
}