<?php
namespace ETC\App\Models\Widgets;

use ETC\App\Models\WC_Widget;

/**
 * Categories filter.
 *
 * @since      1.4.4
 * @package    ETC
 * @subpackage ETC/Models/Admin
 */
if( ! class_exists( 'WC_Widget' ) ) return;
class Categories_Filter extends WC_Widget {

    /**
     * Constructor.
     */
    public function __construct() {
        $this->widget_cssclass    = 'sidebar-widget etheme-brands-filter';
        $this->widget_description = esc_html__( 'Widget to filtering products by categories', 'xstore-core' );
        $this->widget_id          = 'etheme_categories_filter';
        $this->widget_name        = '8theme - ' . esc_html__( 'Filter Products by Categories', 'xstore-core' );
        $this->settings           = array(
            'title' => array(
                'type'  => 'text',
                'std'   => esc_html__( 'Title', 'xstore-core' ),
                'label' => esc_html__( 'Title', 'xstore-core' ),
            ),
            'display_type' => array(
                'type'    => 'select',
                'std'     => 'list',
                'label'   => esc_html__( 'Display type', 'xstore-core' ),
                'options' => array(
                    'list'     => esc_html__( 'List', 'xstore-core' ),
                    'dropdown' => esc_html__( 'Dropdown', 'xstore-core' ),
                ),
            ),
            'count' => array(
                'type'  => 'checkbox',
                'std'   => 0,
                'label' => esc_html__( 'Show product counts', 'xstore-core' )
            ),
        );

        parent::__construct();
    }

    /**
     * Output widget.
     *
     * @see WP_Widget
     *
     * @param array $args Arguments.
     * @param array $instance Instance.
     */
    public function widget( $args, $instance ) {
        if ( ! is_shop() && ! is_product_taxonomy() ) {
            return;
        }

        $_chosen_attributes = \WC_Query::get_layered_nav_chosen_attributes();
        $count              = isset( $instance['count'] ) ? $instance['count'] : $this->settings['count']['std'];
        $title              = isset( $instance['title'] ) ? $instance['title'] : $this->settings['title']['std'];
        $taxonomy           = 'product_cat';
        $query_type         = isset( $instance['query_type'] ) ? $instance['query_type'] : 'and';
        $display_type       = isset( $instance['display_type'] ) ? $instance['display_type'] : $this->settings['display_type']['std'];

        $hide_empty = false;
        $terms_to_exclude =  get_terms(
            array(
                'taxonomy' => 'product_cat',
                'fields'  => 'ids',
                'slug'    => array( 
                    'todas-las-categorias', 
                    'sin-categorizar'
                )
            )
        );
        $terms = get_terms(
            array(
                'taxonomy' => 'product_cat',
                'hide_empty' => $hide_empty,
                'operator'         => 'IN',
                'parent' => 0,
                'exclude' => $terms_to_exclude
            )
        );

        if ( is_wp_error( $terms ) || 0 === count( $terms ) ) {
            return;
        }


        $class = '';
        $shop_url = '';

        if ( is_tax( 'product_cat' ) ) {
           $class = 'on_brand';
           $shop_url = 'data-shop-url="' . get_permalink( wc_get_page_id( 'shop' ) ) . '"';
        }

        $items = '';

        if( count( $terms ) > 0 ) {
            foreach ( $terms as $brand ) {

                $class = 'cat-item';
                $stock = $brand->count;

                if ( $hide_empty && 'yes' === get_option( 'woocommerce_hide_out_of_stock_items' ) && is_tax( 'product_cat' ) ) {
                    global $wp_query;
                    $cat    = $wp_query->get_queried_object();
                    $stock  = etheme_stock_taxonomy( $brand->term_id, 'product_cat', $cat->slug );
                } elseif( $hide_empty && 'yes' === get_option( 'woocommerce_hide_out_of_stock_items' ) ) {
                    $stock  = etheme_stock_taxonomy( $brand->term_id, 'product_cat' );
                } elseif( is_tax( 'product_cat' ) ) {
                    global $wp_query;
                    $cat    = $wp_query->get_queried_object();
                    $stock  = etheme_stock_taxonomy( $brand->term_id, 'product_cat', $cat->slug, false );
                }

                $term_counts  = $this->get_filtered_term_product_counts( wp_list_pluck( $terms, 'term_id' ), $taxonomy, $query_type );

                if ( ! array_key_exists( $brand->term_id, $term_counts) ) {
                    continue;
                }

                $thumbnail_id = absint(get_term_meta($brand->term_id, 'thumbnail_id', true)); 

                $link = remove_query_arg( 'product_cat', $this->get_current_page_url() );


                $current_filter = isset( $_GET['product_cat'] ) ? explode( ',', wc_clean( wp_unslash( $_GET['product_cat'] ) ) ) : array();
                $current_filter = array_map( 'sanitize_title', $current_filter );

                $all_filters = $current_filter;

                if ( ! in_array( $brand->slug, $current_filter, true ) ) {
                    $all_filters[] = $brand->slug;
                } else {
                    $key = array_search( $brand->slug, $all_filters );
                    unset( $all_filters[$key] );
                    $class .= ' current-item';
                }

                if ( ! empty($all_filters) ) {
                    $link = add_query_arg( 'product_cat', implode( ',', $all_filters ), $link );
                }

                $countProd = ( $count == 1 ) ? "({$stock})" : '';

                /**
                 * Subterms
                 */
                $subitems = '';
                $subterms = get_terms(
                    array(
                        'taxonomy' => 'product_cat',
                        'hide_empty' => $hide_empty,
                        'operator'         => 'IN',
                        'parent' => $brand->term_id
                    )
                );
                foreach ( $subterms as $subterm ) {

                    $subclass = 'cat-item';
                    $substock = $subterm->count;
    
                    if ( $hide_empty && 'yes' === get_option( 'woocommerce_hide_out_of_stock_items' ) && is_tax( 'product_cat' ) ) {
                        global $wp_query;
                        $subcat    = $wp_query->get_queried_object();
                        $substock  = etheme_stock_taxonomy( $subterm->term_id, 'product_cat', $subcat->slug );
                    } elseif( $hide_empty && 'yes' === get_option( 'woocommerce_hide_out_of_stock_items' ) ) {
                        $substock  = etheme_stock_taxonomy( $subterm->term_id, 'product_cat' );
                    } elseif( is_tax( 'product_cat' ) ) {
                        global $wp_query;
                        $subcat    = $wp_query->get_queried_object();
                        $substock  = etheme_stock_taxonomy( $subterm->term_id, 'product_cat', $subcat->slug, false );
                    }
    
                    $subterm_counts  = $this->get_filtered_term_product_counts( wp_list_pluck( $subterms, 'term_id' ), $taxonomy, $query_type );
    
                    if ( ! array_key_exists( $subterm->term_id, $subterm_counts) ) {
                        continue;
                    }
    
                    $subthumbnail_id = absint(get_term_meta($subterm->term_id, 'thumbnail_id', true)); 
    
                    $sublink = remove_query_arg( 'product_cat', $this->get_current_page_url() );
    
    
                    $subcurrent_filter = isset( $_GET['product_cat'] ) ? explode( ',', wc_clean( wp_unslash( $_GET['product_cat'] ) ) ) : array();
                    $subcurrent_filter = array_map( 'sanitize_title', $subcurrent_filter );
    
                    $suball_filters = $subcurrent_filter;
    
                    if ( ! in_array( $subterm->slug, $subcurrent_filter, true ) ) {
                        $suball_filters[] = $subterm->slug;
                    } else {
                        $key = array_search( $subterm->slug, $suball_filters );
                        unset( $suball_filters[$key] );
                        $subclass .= ' current-item';
                    }
    
                    if ( ! empty($suball_filters) ) {
                        $sublink = add_query_arg( 'product_cat', implode( ',', $suball_filters ), $sublink );
                    }
    
                    $subcountProd = ( $count == 1 ) ? "({$substock})" : '';
                    $subitems .= '<li class="' . $subclass . '">';         
                        $subitems .= '<a href="' . $sublink . '">';
                            $subitems .= esc_html( $subterm->name );
                            $subitems .= '<span class="count">' . esc_html( $subcountProd ) . '</span>';
                        $subitems .= '</a>';
                    $subitems .= '</li>';
                }
                // Render widget items
                if ( $display_type == 'dropdown' ) {
                    $selected = ( is_tax( 'product_cat' , $brand->term_id ) ) ? ' selected' : '' ;
                    $items .= '<option class="level-0" value="' . esc_html( $brand->name ) . '" data-url="' . $link . '"' . $selected . '>' . esc_html( $brand->name .' '. $countProd ) . '</option>';
                } else {
                    $items .= '<li class="' . $class . '">';         
                        $items .= '<a href="' . $link . '">';
                            $items .= esc_html( $brand->name );
                            $items .= '<span class="count">' . esc_html( $countProd ) . '</span>';
                        $items .= '</a>';
                        $items .= !empty($subitems) ? '<ul style="padding-left: 2em">'.$subitems.'</ul>' : '';
                    $items .= '</li>';
                }
            }
        }

        // Render widget
        $out = '';
        $out .= '<div class="sidebar-widget etheme etheme_widget_brands_filter etheme_widget_brands ' . $class . '" ' . $shop_url . '>';
            if ( $title ) $out .= '<h4 class="widget-title"><span>' . $title . '</span></h4>';
            if ( $display_type == 'dropdown' ) {
                $out .= '<select name="product_cat" class="dropdown_product_cat">';
                    $out .= '<option value="" selected="selected" data-url="">'.esc_html__('Selecciona una categoría', 'xstore-core').'</option>';
                    $out .= '<option value="" data-url="' . get_permalink( wc_get_page_id( 'shop' ) ). '">'.esc_html__('Todas las categorías', 'xstore-core').'</option>';
                        $out .= $items;
                    $out .= '</select>';
                    wc_enqueue_js( "
                        jQuery( '.dropdown_product_cat' ).change( function() {
                            var url = jQuery(this).find( 'option:selected' ).data( 'url' );
                            if ( url != '' ) location.href = url;
                        });
                    " );
            } else {
                $out .= '<ul>';
                    $out .= '<li class="cat-item all-items"><a href="' . get_permalink( wc_get_page_id( 'shop' ) ). '">' . esc_html__('Todas las categorías', 'xstore-core') . '</a></li>';
                    $out .= $items;

                $out .= '</ul>';
            }
        $out .= '</div>';

        echo $out;
    }
    protected function get_filtered_term_product_counts( $term_ids, $taxonomy, $query_type ) {
        global $wpdb;
        $tax_query  = \WC_Query::get_main_tax_query();
        $meta_query = \WC_Query::get_main_meta_query();
        if ( 'or' === $query_type ) {
            foreach ( $tax_query as $key => $query ) {
                if ( is_array( $query ) && $taxonomy === $query['taxonomy'] ) {
                    unset( $tax_query[ $key ] );
                }
            }
        }
        $meta_query     = new \WP_Meta_Query( $meta_query );
        $tax_query      = new \WP_Tax_Query( $tax_query );
        $meta_query_sql = $meta_query->get_sql( 'post', $wpdb->posts, 'ID' );
        $tax_query_sql  = $tax_query->get_sql( $wpdb->posts, 'ID' );
        // Generate query.
        $query           = array();
        $query['select'] = "SELECT COUNT( DISTINCT {$wpdb->posts}.ID ) as term_count, terms.term_id as term_count_id";
        $query['from']   = "FROM {$wpdb->posts}";
        $query['join']   = "
            INNER JOIN {$wpdb->term_relationships} AS term_relationships ON {$wpdb->posts}.ID = term_relationships.object_id
            INNER JOIN {$wpdb->term_taxonomy} AS term_taxonomy USING( term_taxonomy_id )
            INNER JOIN {$wpdb->terms} AS terms USING( term_id )
            " . $tax_query_sql['join'] . $meta_query_sql['join'];
        $query['where'] = "
            WHERE {$wpdb->posts}.post_type IN ( 'product' )
            AND {$wpdb->posts}.post_status = 'publish'"
            . $tax_query_sql['where'] . $meta_query_sql['where'] .
            'AND terms.term_id IN (' . implode( ',', array_map( 'absint', $term_ids ) ) . ')';
        $search = \WC_Query::get_main_search_query_sql();
        if ( $search ) {
            $query['where'] .= ' AND ' . $search;
        }
        $query['group_by'] = 'GROUP BY terms.term_id';
        $query             = apply_filters( 'woocommerce_get_filtered_term_product_counts_query', $query );
        $query             = implode( ' ', $query );
        // We have a query - let's see if cached results of this query already exist.
        $query_hash    = md5( $query );
        // Maybe store a transient of the count values.
        $cache = apply_filters( 'woocommerce_layered_nav_count_maybe_cache', true );
        if ( true === $cache ) {
            $cached_counts = (array) get_transient( 'wc_layered_nav_counts_' . sanitize_title( $taxonomy ) );
        } else {
            $cached_counts = array();
        }
        if ( ! isset( $cached_counts[ $query_hash ] ) ) {
            $results                      = $wpdb->get_results( $query, ARRAY_A ); // @codingStandardsIgnoreLine
            $counts                       = array_map( 'absint', wp_list_pluck( $results, 'term_count', 'term_count_id' ) );
            $cached_counts[ $query_hash ] = $counts;
            if ( true === $cache ) {
                set_transient( 'wc_layered_nav_counts_' . sanitize_title( $taxonomy ), $cached_counts, DAY_IN_SECONDS );
            }
        }
        return array_map( 'absint', (array) $cached_counts[ $query_hash ] );
    }
}