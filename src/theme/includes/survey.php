<?php

add_action('etheme_page_heading', 'rb_check_survey');
function rb_check_survey(){
    $pagename = get_query_var('pagename');
    if($pagename != 'encuesta-de-satisfaccion') return;
    $order_id = $_GET['order_id'];
    $order = wc_get_order( $order_id );

    if(empty($order) || get_current_user_id() != $order->get_user_id()  || $order->get_meta('_rb_survey') ){
        wp_redirect(home_url());
        exit;
    }
    ?>
    <script>
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            setTimeout(function(){
                location = '<?php echo home_url(); ?>';
            }, 5000);
        }, false );
        </script>
    <?php
    
}

add_action( 'woocommerce_thankyou', 'rb_add_survey_control', 10, 1 ); 
function rb_add_survey_control( $order_get_id ) { 
    $order = wc_get_order( $order_get_id );
    if(!$order->get_meta('_rb_survey')):
    ?>
    <div class="text-center">
        <p>
            <a href="<?php echo home_url('encuesta-de-satisfaccion').'?order_id='.$order_get_id; ?>" class="btn active" target="_BLANK">Ir a la encuesta</a>
        </p>
    </div>
    <div id="survey-popup" class="white-popup mfp-hide">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="text-center">Ayudanos a contestar una encuesta</h2>
                    <p class="text-center">Para nosotros es muy importante tu opinión, ayúdanos contestando algunas preguntas</p>
                    <p class="text-center">
                        <a href="<?php echo home_url('encuesta-de-satisfaccion').'?order_id='.$order_get_id; ?>" class="btn active" target="_BLANK">Ir a la encuesta</a>
                    </p>
				</div>
			</div>
		</div>
	</div>
	<script>
		(function($) {
			$(window).load(function () {
				// retrieved this line of code from http://dimsemenov.com/plugins/magnific-popup/documentation.html#api
				$.magnificPopup.open({
					items: {
						src: '#survey-popup',
						type: 'inline'
					},
					callbacks: {
						open: function() {
							$('.mfp-content').addClass('rb-survey-popup');
						// Will fire when this exact popup is opened
						// this - is Magnific Popup object
						}
					}

				// You may add options here, they're exactly the same as for $.fn.magnificPopup call
				// Note that some settings that rely on click event (like disableOn or midClick) will not work here
				}, 0);
			});
		})(jQuery);
	</script>
		
	<?php  
    endif;
}
            


add_action( 'wpcf7_mail_sent', 'set_survey_order', 10, 1 ); 
function set_survey_order( $contact_form ) { 
    $submission = WPCF7_Submission::get_instance();

    if ( $submission ) {
        $posted_data = $submission->get_posted_data();
        if(!empty($posted_data['order_id'])){
            $order = wc_get_order( $posted_data['order_id'] );
            $order->update_meta_data( '_rb_survey', 1 );
            $order->save();
        }
    }
}