<?php

add_action('pre_get_posts', 'gv_search_tags_query');
function gv_search_tags_query($query){
    add_filter('posts_where', 'gv_search_product_tags');
}

function gv_search_product_tags($where = ''){
	global $wp_the_query;
	
	if ( ( !empty( $wp_the_query->query_vars['wc_query'] ) && !empty( $wp_the_query->query_vars['s'] ) ) || !empty($_REQUEST['query']) ){
		global $wpdb;
		$prefix = 'wp_';
		if ( $wpdb->prefix ) {
		   // current site prefix
		   $prefix = $wpdb->prefix;
		} elseif ( $wpdb->base_prefix ) {
		   // wp-config.php defined prefix
		   $prefix = $wpdb->base_prefix;
		}
	
		// ! Filter by tags
		$s = $wp_the_query->query_vars['s'] ? $wp_the_query->query_vars['s'] : $_REQUEST['query'];
		$tags = explode(' ', $s);
		$ids    = array();
		foreach($tags as $tag){
			$term = get_term_by('name', $tag, 'product_tag');
			if ( ! isset( $term->term_taxonomy_id ) || empty( $term->term_taxonomy_id ) ) {
			} else {
				$ids[] = $term->term_taxonomy_id;
			}
		}
		
		if ( ! implode( ',', $ids ) ) {
			$ids = 0;
		} else {
			$ids = implode( ',', $ids );
		}
		
		$where .= " OR " . $prefix . "posts.ID IN ( SELECT " . $prefix . "term_relationships.object_id  FROM " . $prefix . "term_relationships WHERE term_taxonomy_id  IN (" . $ids . ") )";
	}
	return $where;
}