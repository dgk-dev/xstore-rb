<?php

 /**
 * Plugin Name: Billpocket Woocommerce Payment Gateway
 * Plugin URI: https://www.billpocket.com/
 * Description: Provides a credit card payment method with Billbocket for WooCommerce. Compatible with WooCommerce 4.0.0 and Wordpress 5.0.3.
 * Version: 1.0
 * Author: dgk
 * Author URI: https://dgk.com.mx
 * Developer: dgk
 * Text Domain: billpocket-woo-gateway
 *
 * WC requires at least: 3.0
 * WC tested up to: 4.0
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * 
 */

function billpocket_payment_init_your_gateway() {
    if (class_exists('WC_Payment_Gateway')) {
        include_once('billpocket_payment_gateway.php');
    }
}

add_action('plugins_loaded', 'billpocket_payment_init_your_gateway', 0);
add_action('template_redirect', 'wc_custom_redirect_after_purchase', 0);
add_action('woocommerce_order_refunded', 'billpocket_woocommerce_order_refunded', 10, 2);
add_action('woocommerce_api_billpocket_confirm', 'billpocket_woocommerce_confirm', 10, 0);
add_action('admin_enqueue_scripts',  'billpocket_card_load_scripts');          

function billpocket_woocommerce_confirm() {   
    global $woocommerce;
    $billpocket = new Billpocket_Payment();
    $billResponse = $billpocket->validateResponse($_GET);
    if($billResponse !== false){
        $order_id = $billResponse->checkout->externalId;
        $message = $billResponse->transaction->message;
        $order = new WC_Order($order_id);
        if ($order && $message != 'APROBADA') {
            $order->add_order_note(sprintf("%s el pago con tarjeta no pudo realizarse: '%s'", 'Billpocket_Payment', 'Message: '+$message));
            $order->set_status('failed');
            $order->save();

            if (function_exists('wc_add_notice')) {
                wc_add_notice(__('Error en la transacción: No se pudo completar tu pago.'), 'error');
            } else {
                $woocommerce->add_error(__('Error en la transacción: No se pudo completar tu pago.'), 'woothemes');
            }
        } else if ($order && $message == 'APROBADA') {
            $order->payment_complete();
            $woocommerce->cart->empty_cart();
            $order->add_order_note(sprintf("%s pago completado con el id de autorización '%s'", 'Billpocket_Payment', $_GET['authorization']));
        }
        wp_redirect($billpocket->get_return_url($order));
    }else{
        if (function_exists('wc_add_notice')) {
            wc_add_notice(__('Error en la transacción: No se pudo comprobar tu pago. Favor de contactar al soporte técnico si cree que esto es un error.'), 'error');
        } else {
            $woocommerce->add_error(__('Error en la transacción: No se pudo comprobar tu pago. Favor de contactar al soporte técnico si cree que esto es un error.'), 'woothemes');
        }
        wp_redirect(WC()->cart->get_checkout_url());
    }
}    

function wc_custom_redirect_after_purchase() {
    global $wp;
    $logger = wc_get_logger();

    if (is_checkout() && !empty($wp->query_vars['order-received'])) {
        $order = new WC_Order($wp->query_vars['order-received']);
        $redirect_url = get_post_meta($order->get_id(), '_billpocket_checkout_url', true);

        if ($redirect_url && $order->get_status() == 'pending') {
            $logger->debug($redirect_url);
            wp_redirect($redirect_url);
            exit();
        }
    }
}

/**
 * Realiza el reembolso de la orden en Openpay
 * 
 * @param type $order_id
 * @param type $refund_id
 * 
 * @link https://docs.woocommerce.com/wc-apidocs/source-function-wc_create_refund.html#587
 */
function billpocket_woocommerce_order_refunded($order_id, $refund_id) { 
    // $logger = wc_get_logger();                
    // $logger->info('ORDER: '.$order_id);             
    // $logger->info('REFUND: '.$refund_id); 
    
    // $order  = wc_get_order($order_id);
    // $refund = wc_get_order($refund_id);
    
    // if ($order->get_payment_method() != 'openpay_cards') {
    //     $logger->info('get_payment_method: '.$order->get_payment_method());             
    //     return;
    // }

    // $transaction_id = get_post_meta($order_id, '_openpay_transaction_id', true);
    
    // $reason = $refund->get_reason() ? $refund->get_reason() : 'Refund ID: '.$refund_id;
    // $amount = number_format((float)$refund->get_amount(), 2, '.', '');
        
    // $logger->info('_transaction_id: '.$transaction_id);             

    // try {
    //     $openpay_cards = new Openpay_Cards();    

    //     $bbva = $openpay_cards->getBbvaInstance();
    //     $charge = $bbva->charges->get($transaction_id);
    //     $charge->refund(array(
    //         'description' => $reason,
    //         'amount' => $amount                
    //     ));
    //     $order->add_order_note('Payment was also redunded in Openpay');
    // } catch (Exception $e) {
    //     $logger->error($e->getMessage());             
    //     $order->add_order_note('There was an error refunding charge in Openpay: '.$e->getMessage());
    // }        

    return;
} 


function billpocket_card_load_scripts() {
    wp_enqueue_script('cardScript', plugin_dir_url( __FILE__ ).'assets/js/cardScript.js');
}