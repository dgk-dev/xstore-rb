<?php

/*
  Title:	Billpocket Payment extension for WooCommerce
  Author:	dgk
  URL:		https://dgk.com.mx
  License: GNU General Public License v3.0
  License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

class Billpocket_Payment extends WC_Payment_Gateway
{

    protected $GATEWAY_NAME = "Billpocket payment gateway";
    protected $is_sandbox = true;
    protected $order = null;
    protected $transaction_id = null;
    protected $transactionErrorMessage = null;
    protected $currencies = array('MXN');

    public function __construct() {        
        $this->id = 'billpocket_payment';
        $this->method_title = __('Billpocket Payment', 'billpocket-woo-gateway');
        $this->has_fields = true;

        $this->init_form_fields();
        $this->init_settings();
        $this->logger = wc_get_logger();
        
        

        $this->title = $this->settings['title'] ? $this->settings['title'] : 'Pago con tarjeta de crédito o débito a través de Billpocket.';
        $this->description = '';        
        $this->is_sandbox = strcmp($this->settings['sandbox'], 'yes') == 0;
        $this->sandbox_api_key = $this->settings['sandbox_api_key'];
       
        $this->production_api_key = $this->settings['production_api_key'];
       
        $this->api_key = $this->is_sandbox ? $this->sandbox_api_key : $this->production_api_key;

        $this->api_url = $this->is_sandbox ? 'https://test.paywith.billpocket.com/api/v1/checkout/' : 'https://paywith.billpocket.com/api/v1/checkout/';
        $this->checkout_url = $this->is_sandbox ? 'https://test.paywith.billpocket.com/checkout/' : 'https://paywith.billpocket.com/checkout/';
       

        if ($this->is_sandbox) {
            $this->description .= __('SANDBOX MODE ENABLED. In test mode', 'billpocket-woo-gateway');
        }
        
        if (!$this->validateCurrency()) {
            $this->enabled = false;
        }                        
        
        add_action('wp_enqueue_scripts', array($this, 'payment_scripts'));
        add_action('woocommerce_update_options_payment_gateways_'.$this->id, array($this, 'process_admin_options'));
        add_action('admin_notices', array(&$this, 'perform_ssl_check'));        
        add_action('woocommerce_checkout_create_order', array($this, 'action_woocommerce_checkout_create_order'), 10, 2);                
    }       
    
    /**
     * Si el tipo de cargo esta configurado como Pre-autorización, el estatus de la orden es marcado como "on-hold"
     * 
     * @param type $order
     * @param type $data
     * 
     * @link https://docs.woocommerce.com/wc-apidocs/source-class-WC_Checkout.html#334
     */
    public function action_woocommerce_checkout_create_order($order, $data) {        
        if ($order->get_payment_method() == 'billpocket_payment') {
            $order->set_status('pending');            
        }        
    } 
    
    public function perform_ssl_check() {
        if (!$this->is_sandbox && !is_ssl() && $this->enabled == 'yes') :
            echo '<div class="error"><p>'.sprintf(__('El modo de pruebas (sandbox) de %s está desactivado y puede realizar operaciones en producción, sin embargo el protocolo actual no es seguro. Favor de asegurarse que se cuenta con un certificado SSL válido.', 'billpocket-woo-gateway'), $this->GATEWAY_NAME, admin_url('admin.php?page=settings')).'</p></div>';
        endif;
    }

    public function init_form_fields() {
        $this->form_fields = array(
            'enabled' => array(
                'type' => 'checkbox',
                'title' => __('Habilitar módulo', 'billpocket-woo-gateway'),
                'label' => __('Habilitar', 'billpocket-woo-gateway'),
                'default' => 'yes'
            ),
            'title' => array(
                'type' => 'text',
                'title' => __('Título en checkout', 'billpocket-woo-gateway'),
                'description' => __('', 'billpocket-woo-gateway'),
                'default' => __('Pago con tarjeta de crédito o débito a través de Billpocket.', 'billpocket-woo-gateway')
            ),        
            'sandbox' => array(
                'type' => 'checkbox',
                'title' => __('Modo de pruebas (SANDBOX)', 'billpocket-woo-gateway'),
                'label' => __('Habilitar', 'billpocket-woo-gateway'),                
                'default' => 'no'
            ),
            'sandbox_api_key' => array(
                'type' => 'text',
                'title' => __('API KEY Sandbox', 'billpocket-woo-gateway'),
                'description' => __('', 'billpocket-woo-gateway'),
                'default' => __('', 'billpocket-woo-gateway')
            ),
            'production_api_key' => array(
                'type' => 'text',
                'title' => __('API KEY Producción', 'billpocket-woo-gateway'),
                'description' => __('', 'billpocket-woo-gateway'),
                'default' => __('', 'billpocket-woo-gateway')
            )
        );
    }

    public function admin_options() {
        include_once('templates/admin.php');
    }

    public function payment_fields() {
        $this->images_dir = plugin_dir_url( __FILE__ ).'/assets/images/';
        include_once('templates/payment_billpocket.php');
    }
    
    /**
     * payment_scripts function.
     *
     * Outputs scripts used for openpay payment
     *
     * @access public
     */
    public function payment_scripts() {
        //enqueue if neccesary
    }
    
    private function getProductsDetail() {
        $order = $this->order;
        $products = [];
        foreach( $order->get_items() as $item_product ){                        
            $product = $item_product->get_product();                        
            $products[] = $product->get_name();
        }
        return $products;
    }

    public function process_payment($order_id) {
        global $woocommerce;
        $this->order = new WC_Order($order_id);
                        
        $amount = number_format((float)$this->order->get_total(), 2, '.', '');

        $chargeRequest = array(
            'apiKey' => $this->api_key,
            'externalId' => $order_id,
            'items' => $this->getProductsDetail(),
            'total' => $amount
        );

        $postArgs = array(
            'headers'     => array('Content-Type' => 'application/json; charset=utf-8'),
            'body'        => json_encode($chargeRequest),
        );

        $request = wp_remote_post( $this->api_url, $postArgs );

        if (  is_wp_error( $request ) ) {
            $error_message = $request->get_error_message();
            $this->error($error_message);
        } else {
            $response = json_decode(wp_remote_retrieve_body( $request ));
            update_post_meta($this->order->get_id(), '_billpocket_checkout_id', $response->checkoutId);
            update_post_meta($this->order->get_id(), '_billpocket_checkout_url', $this->checkout_url.$response->checkoutId);
            $woocommerce->cart->empty_cart();            
            return array(
                'result' => 'success',
                'redirect' => $this->get_return_url($this->order)
            );
        }
    }

    public function error($e) {
        global $woocommerce;
        $error = 'ERROR '.$e;        
        $this->transactionErrorMessage = $error;
        if (function_exists('wc_add_notice')) {
            wc_add_notice($error, 'error');
        } else {
            $woocommerce->add_error(__('Error de pago: ', 'billpocket-woo-gateway').$error);
        }
    }

    /**
     * Checks if woocommerce has enabled available currencies for plugin
     *
     * @access public
     * @return bool
     */
    public function validateCurrency() {
        return in_array(get_woocommerce_currency(), $this->currencies);
    }

    public function validateResponse($response) {
        if(empty($response['checkout']) || empty($response['externalId']) || empty($response['signature'])) return false;
        
        $request = wp_remote_get( $this->api_url.'detail/'.$response['checkout'].'?apiKey='.$this->api_key );
        
        if (  is_wp_error( $request ) ) {
            return false;
        } else {
            $billResponse = json_decode(wp_remote_retrieve_body( $request ));
            return ($billResponse->checkout->externalId == $response['externalId'] && $billResponse->transaction->signature == $response['signature']) ? $billResponse : false;
        }
    }

}

function billpocket_payment_add_creditcard_gateway($methods) {
    array_push($methods, 'billpocket_payment');
    return $methods;
}

add_filter('woocommerce_payment_gateways', 'billpocket_payment_add_creditcard_gateway');

