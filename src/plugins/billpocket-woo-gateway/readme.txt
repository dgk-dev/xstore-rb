=== Billpocket Payment Plugin ===
Contributors: dgk
Tags: payment gateway, billpocket
Requires at least: 4.8
Tested up to: 5.0.3
Requires PHP: 5.6
Stable tag: 1.0
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Provides a credit card payment method with Billpocket for WooCommerce. Compatible with WooCommerce 4.0.0 and Wordpress 5.0.3.