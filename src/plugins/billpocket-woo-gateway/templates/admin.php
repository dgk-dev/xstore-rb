<?php
/*
  Title:	Billpocket Payment extension for WooCommerce
  Author:	dgk
  URL:		https://dgk.com.mx
  License: GNU General Public License v3.0
  License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
?>

<h3>
    <?php _e('Billpocket Payment', 'billpocket-woo-gateway'); ?>
</h3>

<?php if(!$this->validateCurrency()): ?>
    <div class="inline error">Billpocket Payment Plugin is only available for MXN currency.</div>
<?php endif; ?>

<p><?php _e('Billpocket Payment settings.', 'billpocket-woo-gateway'); ?></p>


<table class="form-table">
    <?php $this->generate_settings_html(); ?>
</table>
