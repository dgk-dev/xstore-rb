<?php
/*
  Title:	Billpocket Payment extension for WooCommerce
  Author:	dgk
  URL:		https://dgk.com.mx
  License: GNU General Public License v3.0
  License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
?>
<style>
    .form-row{
        margin: 0 0 6px !important;
        padding: 3px !important;
    }
    
    .form-row select{
        width: 100% !important;
    }
</style>

<div style="overflow: hidden;">
    <div>
        <div style="width: 100%;">
            <h5 style="margin-bottom: 0">Tarjetas de crédito:</h5>	
            <img alt="" src="<?php echo $this->images_dir ?>credit_cards.png" style="display: block; margin-bottom: 1em">	
        </div>
        <div style="width: 100%;">
            <h5 style="margin-bottom: 0">Tarjetas de débito:</h5>	
            <img alt="" src="<?php echo $this->images_dir ?>debit_cards.png" style="display: block; margin-bottom: 1em">	
        </div>
    </div>	
    <p>Serás redireccionado a la plataforma de pago seguro de Billpocket.</p>
</div>
<div style="text-align: center">
    <img alt="" src="<?php echo $this->images_dir ?>billpocket.png">	
</div>