(function ($) {
    $(document).ready(function(){
        var fadeSwiper = document.querySelector('.swiper-fade .swiper-container');
        if(fadeSwiper !== null){
            fadeSwiper = document.querySelector('.swiper-fade .swiper-container').swiper;
            fadeSwiper.params.effect = 'fade';
            fadeSwiper.params.fadeEffect = {crossFade: true};
            fadeSwiper.update();
        }
    });

    $(window).load(function(){
        /**
         * Funciones de formulario de citas
         */
        var $sendFormButton = $('.ea-submit');
        if($sendFormButton.length){
            $sendFormButton.addClass('vc_btn3');
            $sendFormButton.html('GENERAR CITA <i class="vc_btn3-icon fas fa-angle-right"></i>')
            $sendFormButton.removeClass('ea-btn');
        }

        var $drop_uploader = $('.drop_uploader');

        if($drop_uploader.length){
            Number.prototype.toFixed = function(decimalPlaces) {
                return this;
            };
        }

        var $sendFormButton = $('.ea-submit');
        if($sendFormButton.length){
            $sendFormButton.addClass('vc_btn3');
            $sendFormButton.html('GENERAR CITA <i class="vc_btn3-icon fas fa-angle-right"></i>')
            $sendFormButton.removeClass('ea-btn');
        }

        var $subjectSelect = $('select[name="asunto"]');
        if($subjectSelect.length){
            $subjectSelect.on('change', function(){
                var value = $subjectSelect.val();
                if(value === "Recoger mercancía"){
                    $subjectSelect.after('<div class="message" style="color: red; white-space: normal">Para poderte entregar tus artículos necesitamos que nos entregues una fotocopia de tu identificación</div>');
                }else{
                    $subjectSelect.next('div.message').remove();
                }
            });
        }

        $('.ea-bootstrap .custom-field').on('focusout keyup', function(){
            var $element = $(this);
            if ( $element.is(':input') && $element.attr('data-rule-email') ) {
                $element.val($.trim($element.val()));
            }
        });

        /**
         * Funciones quiero vender
         */
        $(document).on('click', '.wpcf7-submit', function(){
            setTimeout(function () {
                $('.wpcf7-submit').attr('disabled', true);
            }, 5);
        })
        $(document).on( 'file_upload_start', function() {
            setTimeout(function () {
                $('.wpcf7-submit').attr('disabled', 'disabled');
            }, 5);
        });
        $(document).on( 'wpcf7submit file_upload_end', function() {
            setTimeout(function () {
                if(!cf7du_currently_uploading) $('.wpcf7-submit').removeAttr('disabled');
            }, 5);
        });
    });
})(jQuery);